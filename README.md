<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

## About Stuck Neverflow

Stuck Neverflow is a forum application created using the Laravel web development framework, this application uses a template from SBadmin

## Home Page Stuck Neverflow
<p><a href="https://ibb.co/n01pLkQ"><img src="public/img/ss/homepage.png" alt="homepage" border="0"></a></p>

## Register Page Stuck Neverflow
<p><a href="https://ibb.co/GnNmCx0"><img src="public/img/ss/register.png" alt="register" border="0"></a></p>

## Login Page Stuck Neverflow
<p><a href="https://ibb.co/DtLJFDw"><img src="public/img/ss/login.png" alt="login" border="0"></a></p>

## User Level Access
Stuck neverflow have 3 level access
- [unsigned]
- [member]
- [admin]

## Dashboard
-[admin]
<p><a href="https://ibb.co/P6SwBmJ"><img src="public/img/ss/dashboard_admin.png" alt="dashboard-admin" border="0"></a></p>

-[member]
<p><a href="https://ibb.co/qxmjYZG"><img src="public/img/ss/dashboard_member.png" alt="dashboard-member" border="0"></a></p>

## User Detail and Update Form
<p><a href="https://ibb.co/6vFDgyp"><img src="public/img/ss/user_detail.png" alt="user-detail" border="0"></a></p>

## Question
